/*
 * Main.cpp
 *
 *  Created on: Fall 2019
 */

#include <stdio.h>
#include <math.h>
#include <CImg.h>
#include <time.h>
#include <immintrin.h> // Required to use intrinsic functions
#include <malloc.h>
#include <xmmintrin.h>

using namespace cimg_library;

// Data type for image components
typedef float data_t;

const char* SOURCE_IMG      = "hojas_2.bmp";  // The name of the source file to load
const char* BACKGROUND_IMG  = "background_bw_l_7.bmp"; // The name of the background file to load
const char* DESTINATION_IMG = "processed_image.bmp"; // The name of the destination file to save

const int ITEMS_PER_PACKET (sizeof(__m128) / sizeof(data_t));  // Number of items per packet

int main() {

	if(access(SOURCE_IMG, F_OK)) {  // Makes sure source image is available
		perror("Source photo not found!\n");
		exit(EXIT_FAILURE);
	}

	if(access(BACKGROUND_IMG, F_OK)) {  // Makes sure background image is available
        perror("Background photo not found!\n");
		exit(EXIT_FAILURE);
    }

    // Open file
	CImg<data_t> srcImage(SOURCE_IMG);
	CImg<data_t> bckImage(BACKGROUND_IMG);

	if (srcImage.width() != bckImage.width() || srcImage.height() != bckImage.height()) {  // Makes sure both source images have the same dimensions
		perror("Source photos dimensions do not match\n");
		exit(EXIT_FAILURE);
	}

	// Object initialization
	data_t *pDstImage; // Pointer to the new image pixels
	data_t *pRsrc, *pGsrc, *pBsrc; // Pointers to the R, G and B components of source image
	data_t *pRbck, *pGbck, *pBbck; // Pointers to the R, G and B components of background image
	data_t *pRdest, *pGdest, *pBdest; // Pointers to the R, G and B components of destiny image
	uint width, height; // Width and height of the image
	uint nComp; // Number of image components

	// Inicialization of the necessary operands for the algorithm
    __m128 loadRsource, loadGsource, loadBsource;  // Packets where an array of pixels from each color will be loaded (from the source photo)
    __m128 loadRback, loadGback, loadBback; // Packets where an array of pixels from each color will be loaded (from the background photo)
    __m128 subRcomp, subGcomp, subBcomp; // Packets where the result of the subtraction will be saved from each color
    __m128 multRcomp, multGcomp, multBcomp; // Packets where the result of the multiplication will be saved from each color
    __m128 addRcomp, addGcomp, addBcomp; // Packets where the result of the sum will be saved from each color
    __m128 divRcomp, divGcomp, divBcomp; // Packets where the result of the division will be saved from each color
    __m128 resultRcomp, resultGcomp, resultBcomp; // Packets where the result of the whole operation will be saved from each color
    __m128 value1;  // A packet with all of its components inicialiced to the value 1
    __m128 value255; // A packet with all of its components inicialiced to the value 255
    __m128 value256; // A packet with all of its components inicialiced to the value 256

    uint repetitions = 45;	// Times the algorithm is going to be repeated until reaching te time expected
	struct timespec tStart; // Object that will be used to save the inicial time of the algorithm
	struct timespec tEnd;   // Object that will be used to save the final time of the algorithm
	double dElapsedTimeS;   // The time the algorithm takes to finish (calculated by subtracting to the time in tEnd the time in tStart)
	bool controlLastPacket;	// Element to check if we have empty elements in the last packet
	int iterations;			// The number of iterations for the packets of each component

	srcImage.display(); // Displays the source image
	bckImage.display(); // Displays the background image
	width  = srcImage.width(); // Getting information from the source image
	height = srcImage.height();
	nComp  = srcImage.spectrum(); // source image number of components
				// Common values for spectrum (number of image components):
				//  B&W images = 1
				//	Normal color images = 3 (RGB)
				//  Special color images = 4 (RGB and alpha/transparency channel)
	
    int size = width * height * nComp;  // The total size of the array that represents the image

    int nPackets = (size * sizeof(data_t))/sizeof(__m128);  // Number of packets that will be used
	int nRest = (size * sizeof(data_t))%sizeof(__m128); // Number of the rest of elemsts in last packet or if it is 0 is full

    if ( nRest != 0) {  // If the division is not exact, we will need an extra packet for the remaining elements
        nPackets++;		
		controlLastPacket = true;
		iterations = (nPackets/nComp)-1;
    } else {
		controlLastPacket = false;
		iterations = (nPackets/nComp);
	}

    // Allocate memory space for destination image components
	pDstImage = (data_t *)_mm_malloc(sizeof(__m128) * nPackets, sizeof(__m128));
	if (pDstImage == NULL) {
		perror("Allocating destination image");
		exit(-2);
	}

	// Pointers to the componet arrays of the source image
	pRsrc = srcImage.data(); // pRcomp points to the R component array
	pGsrc = pRsrc + height * width; // pGcomp points to the G component array
	pBsrc = pGsrc + height * width; // pBcomp points to B component array

	// Pointers to the componet arrays of the background image
	pRbck = bckImage.data(); // pRcomp points to the R component array
	pGbck = pRbck + height * width; // pGcomp points to the G component array
	pBbck = pGbck + height * width; // pBcomp points to B component array

	// Pointers to the RGB arrays of the destination image
	pRdest = pDstImage;
	pGdest = pRdest + height * width;
	pBdest = pGdest + height * width;

    /***********************************************
	 *   Algorithm start.
	 *   - Measure initial time
	 */
	if (clock_gettime(CLOCK_REALTIME, &tStart) != 0)
	{
		perror("gettime");
		exit(EXIT_FAILURE);
	}

	for(uint times= 0 ; times < repetitions ; times++){  // Loop that marks the times the algorithm is repeated

    	value1 = _mm_set_ps1(1);
    	value255 = _mm_set_ps1(255);
    	value256 = _mm_set_ps1(256);

        for (int i = 0; i < iterations ; i ++){   // Loops for the packets of a single component of the image
            
			// Loading of the R, G, and B components of the source image
			loadRsource = _mm_load_ps((data_t *)&pRsrc[i * ITEMS_PER_PACKET]);
			loadGsource = _mm_load_ps((data_t *)&pGsrc[i * ITEMS_PER_PACKET]);
            loadBsource = _mm_load_ps((data_t *)&pBsrc[i * ITEMS_PER_PACKET]);

			// Loading of the R, G, and B components of the background image
            loadRback = _mm_load_ps((data_t *)&pRbck[i * ITEMS_PER_PACKET]);
            loadGback = _mm_load_ps((data_t *)&pGbck[i * ITEMS_PER_PACKET]);
            loadBback = _mm_load_ps((data_t *)&pBbck[i * ITEMS_PER_PACKET]);

            addRcomp = _mm_add_ps(loadRsource, value1);  // addRcomp = Source(R) + 1
            addGcomp = _mm_add_ps(loadGsource, value1);  // addGcomp = Source(G) + 1
            addBcomp = _mm_add_ps(loadBsource, value1);  // addBcomp = Source(B) + 1

            subRcomp = _mm_sub_ps(value255, loadRback);  // subRcomp = 255 - Background(R)
            subGcomp = _mm_sub_ps(value255, loadGback);  // subGcomp = 255 - Background(G)
            subBcomp = _mm_sub_ps(value255, loadBback);  // subBcomp = 255 - Background(B)

            multRcomp = _mm_mul_ps(value256, subRcomp);  // multRcomp = 256 * (255 - Background(R))
            multGcomp = _mm_mul_ps(value256, subGcomp);  // multGcomp = 256 * (255 - Background(G))
            multBcomp = _mm_mul_ps(value256, subBcomp);  // multBcomp = 256 * (255 - Background(B))

            divRcomp = _mm_div_ps(multRcomp, addRcomp);  // divRcomp = (256 * (255 - Background(R)))/(Source(R) + 1)
            divGcomp = _mm_div_ps(multGcomp, addGcomp);  // divGcomp = (256 * (255 - Background(G)))/(Source(G) + 1)
            divBcomp = _mm_div_ps(multBcomp, addBcomp);  // divBcomp = (256 * (255 - Background(B)))/(Source(B) + 1)

            resultRcomp = _mm_sub_ps(value255, divRcomp);  // resultRcomp = 255 - (256 * (255 - Background(R)))/(Source(R) + 1)
            resultGcomp = _mm_sub_ps(value255, divGcomp);  // resultGcomp = 255 - (256 * (255 - Background(G)))/(Source(G) + 1)
            resultBcomp = _mm_sub_ps(value255, divBcomp);  // resultBcomp = 255 - (256 * (255 - Background(B)))/(Source(B) + 1)

			// Storing of the R, G, and B components of the destination image
            _mm_storeu_ps((data_t *)&pRdest[i * ITEMS_PER_PACKET], resultRcomp);
            _mm_storeu_ps((data_t *)&pGdest[i * ITEMS_PER_PACKET], resultGcomp);
            _mm_storeu_ps((data_t *)&pBdest[i * ITEMS_PER_PACKET], resultBcomp);

        }

		if (controlLastPacket) { //if the last packet is not completed we have to process it element by element
			for (int i = 0; i < nRest; i++) {
				*(pRdest + i + iterations) = 255 - ( ( 256 * ( 255 - *(pRbck + i + (iterations)) ) ) / ( *(pRsrc + i + iterations) + 1 ) ); // Applies the algorithm for the red component

				*(pGdest + i + iterations) = 255 - ( ( 256 * ( 255 - *(pGbck + i + (iterations)) ) ) / ( *(pGsrc + i + iterations) + 1 ) ); // Applies the algorithm for the green component

				*(pBdest + i + iterations) = 255 - ( ( 256 * ( 255 - *(pBbck + i + (iterations)) ) ) / ( *(pBsrc + i + iterations) + 1 ) ); // Applies the algorithm for the blue component
			}
		}
    }

    /***********************************************
	 *  End of the algorithm.
	 *   - Measure the end time
	 *   - Calculate the elapsed time
	 */
	if (clock_gettime(CLOCK_REALTIME, &tEnd) != 0)
	{
		perror("gettime");
		exit(EXIT_FAILURE);
	}

	dElapsedTimeS = tEnd.tv_sec - tStart.tv_sec;
	dElapsedTimeS += (tEnd.tv_nsec - tStart.tv_nsec) / 1e+9;
	printf("Elapsed time: %f\n", dElapsedTimeS);
		
	// Create a new image object with the calculated pixels
	// In case of normal color images use nComp=3,
	// In case of B/W images use nComp=1.
	CImg<data_t> dstImage(pDstImage, width, height, 1, nComp);

	// The pixels of the image are saturated to 0 if their value is lower or to 255 if their value is higher
	dstImage.cut(0,255);

	// Store destination image in disk
	dstImage.save(DESTINATION_IMG); 

	// Display destination image
	dstImage.display();
	
	// Free memory
	_mm_free(pDstImage);

	return 0;
}
